const crypto = require("crypto");
const rand = require("random-number-generator");
const express = require("express");
const bodyParser = require("body-parser");
const pug = require("pug");
const app = express();

const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(express.static(__dirname + "/views"));

app.set("view engine", "pug");

app.listen(process.env.PORT || 3000, () => console.log("listening"));

app.get("/", (req, res) => {
    res.render("index", {
        title: "Random number",
        message: "Devine le nombre entre 1 et 10",
        count: count
    });
});

// Je génère le nombre aléatoire et je le stocke dans une variable
const randomN = rand(10, 1);
console.log(randomN);

// Ce compteur nous servira à vérifier le nombre d'essais
let count = 1;

app.post("/", urlencodedParser, (req, res) => {
    console.log(`Choix de l'utilisateur : ${req.body.choix}`);

    // Je modifie la réponse renvoyée à l'utilisateur selon son choix
    let response;
    if (Number(req.body.choix) && Number(req.body.choix) <= 10) {
        // J'augmente le nombre d'essais à chaque fois que l'utilisateur clique sur "Envoyer"
        if (count < 3) count++;

        if (req.body.choix == randomN) {
            response = "C'est ça !";
        } else if (req.body.choix < randomN && count <= 3) {
            response = "Trop petit !";
        } else if (req.body.choix > randomN && count <= 3) {
            response = "Trop grand !";
        } else {
            response = "T'as perdu !";
        }
    } else {
        response = "C'est pas un nombre entre 1 et 10 !";
    }

    res.render("index", {
        title: "Random number",
        message: "Devine le nombre entre 1 et 10",
        response: response,
        count: count
    });
});
